# Clean Insights SDK

## 2.8.0
- Added `CleanInsights#autoTrack` initializer with reduced configuration
  options and automatic tracking of app starts.


## 2.7.0
- Replaced `Instant` with deprecated `Calendar` to increase backwards compatibility and remove
  desugaring and multi-dex necessity.

## 2.6.1
- Updated dependencies. Build with API 33.
- Added info about necessary desugaring to support APIs older than 26.

## 2.6.0
- Added `consent(feature)`, `consent(campaignId)` and `state(feature)`, `state(campaignId)`
  methods to get detailed consent information.
- Updated dependencies.

## 2.5.2
- Fixed massive bugs when sending data to server. Could have never worked before. 🤦  
- Updated dependencies.

## 2.5.1
- Fixed `IOException` in `DefaultStore#send` on server error.
- Updated dependencies.

## 2.5.0

- Relaxed anonymity guarantees somewhat by allowing to start measuring right away by default, 
  not just at the next period. Added "strengthenAnonymity" toggle to switch back to old behaviour.
- Updated dependencies.

## 2.4.4

- Added `Configuration#check` to check for most common configuration mistakes.

## 2.4.3

- Added `testServer` helper method to `CleanInsights` object, to test for server issues without 
  needing to go through  a full measurement cycle.

## 2.4.2

- Fixed logic of `measureVisit` and `measureEvent` to make sure, `persistAndSend` gets called.
- Updated to latest Kotlin version.

## 2.4.1

- Fixed serious bug when trying to serialize an Insights object. Insights could never be offloaded 
  until now.
- Improved coroutine usage.
- Updated dependencies. 

## 2.4.0

- Added truncated exponential backoff retries on server failures.
- Added automatic purge of too old unsent data.

## 2.3.0

- Renamed `CleanInsights.featureSize` to `featureConsentsSize` and `CleanInsights.campaignSize` to 
  `campaignConsentsSize` for clarification.
- Added some unit tests.

## 2.2.5

- Improved Java interoperability.
- Let DefaultStore override by users. 

## 2.2.4

- Initial working release. 
