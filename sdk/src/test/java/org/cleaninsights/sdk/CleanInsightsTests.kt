package org.cleaninsights.sdk

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import java.io.File
import java.io.IOException
import java.net.URL
import java.util.*
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.jvm.isAccessible

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@OptIn(ExperimentalCoroutinesApi::class)
@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner::class)
class CleanInsightsTests: ConsentRequestUi {

    companion object {
        private val calendar = CalendarAdapter()

        private val conf = Configuration(
                server = URL("http://localhost:8080/ci/cleaninsights.php"),
                siteId = 1,
                campaigns = mapOf(Pair("test", Campaign(
                        start = calendar.fromJson("2021-01-01T00:00:00+00:00"),
                        end = calendar.fromJson("2099-12-31T23:59:59Z"),
                        aggregationPeriodLength = 1,
                        numberOfPeriods = 90,
                        onlyRecordOnce = false,
                        eventAggregationRule = EventAggregationRule.Avg,
                        strengthenAnonymity = false))),
                timeout = 1.0,
                maxRetryDelay = 1.0,
                maxAgeOfOldData = 1,
                persistEveryNTimes = 10,
                serverSideAnonymousUsage = false,
                debug = true)

        private const val confString = "{\n" +
                "    \"server\": \"http://localhost:8080/ci/cleaninsights.php\",\n" +
                "    \"siteId\": 1,\n" +
                "    \"timeout\": 1,\n" +
                "    \"maxRetryDelay\": 1,\n" +
                "    \"maxAgeOfOldData\": 1,\n" +
                "    \"debug\": true,\n" +
                "    \"campaigns\": {\n" +
                "        \"test\": {\n" +
                "            \"start\": \"2021-01-01T00:00:00Z\",\n" +
                "            \"end\": \"2099-12-31T23:59:59Z\",\n" +
                "            \"aggregationPeriodLength\": 1,\n" +
                "            \"numberOfPeriods\": 90,\n" +
                "            \"onlyRecordOnce\": false,\n" +
                "            \"eventAggregationRule\": \"avg\",\n" +
                "            \"strengthenAnonymity\": false\n" +
                "        }\n" +
                "    }\n" +
                "}\n"
    }


    private lateinit var ci: CleanInsights

    @get:Rule
    val tempDir = TemporaryFolder()

    @Before
    fun setup() {
        if (storeFile.exists()) {
            assertTrue(storeFile.delete())
        }

        ci = CleanInsights(confString, tempDir.root)
    }


    @Test
    fun conf_isCorrectlyParsed() {
        assertEquals(conf, ci.conf)
    }

    @Test
    fun denyConsent() {
        assertEquals(ci.state(Feature.Lang), Consent.State.Unknown)
        assertEquals(ci.state(Feature.Ua), Consent.State.Unknown)
        assertEquals(ci.state("test"), Consent.State.Unknown)

        ci.deny(Feature.Lang)
        ci.deny(Feature.Ua)
        ci.deny("test")

        assertEquals(1, ci.campaignConsentSize)
        assertEquals(2, ci.featureConsentSize)

        var consent: Consent? = ci.getFeatureConsentByIndex(0)
        assertNotNull(consent)
        assertFalse(consent!!.granted)
        assertEquals(ci.state(Feature.Lang), Consent.State.Denied)

        consent = ci.getFeatureConsentByIndex(1)
        assertNotNull(consent)
        assertFalse(consent!!.granted)
        assertEquals(ci.state(Feature.Ua), Consent.State.Denied)

        consent = ci.getFeatureConsentByIndex(2)
        assertNull(consent)

        consent = ci.getCampaignConsentByIndex(0)
        assertNotNull(consent)
        assertFalse(consent!!.granted)

        consent = ci.getCampaignConsentByIndex(1)
        assertNull(consent)

        assertFalse(ci.isCampaignCurrentlyGranted("test"))
        assertEquals(ci.state("test"), Consent.State.Denied)
    }

    @Test
    fun grantConsent() {
        assertEquals(ci.state(Feature.Lang), Consent.State.Unknown)
        assertEquals(ci.state(Feature.Ua), Consent.State.Unknown)
        assertEquals(ci.state("test"), Consent.State.Unknown)

        ci.grant(Feature.Lang)
        ci.grant(Feature.Ua)
        ci.grant("test")

        assertEquals(1, ci.campaignConsentSize)
        assertEquals(2, ci.featureConsentSize)

        var consent: Consent? = ci.getFeatureConsentByIndex(0)
        assertNotNull(consent)
        assertTrue(consent!!.granted)
        assertEquals(ci.state(Feature.Lang), Consent.State.Granted)

        consent = ci.getFeatureConsentByIndex(1)
        assertNotNull(consent)
        assertTrue(consent!!.granted)
        assertEquals(ci.state(Feature.Ua), Consent.State.Granted)

        consent = ci.getFeatureConsentByIndex(2)
        assertNull(consent)

        val midnightToday = CleanInsights.now()
        midnightToday.set(Calendar.HOUR_OF_DAY, 0)
        midnightToday.set(Calendar.MINUTE, 0)
        midnightToday.set(Calendar.SECOND, 0)
        midnightToday.set(Calendar.MILLISECOND, 0)

        consent = ci.getCampaignConsentByIndex(0)
        assertNotNull(consent)
        assertTrue(consent!!.startDate == midnightToday.time || consent.startDate.after(midnightToday.time))
        assertTrue(consent.granted)

        consent = ci.getCampaignConsentByIndex(1)
        assertNull(consent)

        assertTrue(ci.isCampaignCurrentlyGranted("test"))
        assertEquals(ci.state("test"), Consent.State.Granted)
    }

    @Test
    fun grantConsentStrengthenedAnonymity() {
        ci = strengthenedCi

        assertEquals(ci.state(Feature.Lang), Consent.State.Unknown)
        assertEquals(ci.state(Feature.Ua), Consent.State.Unknown)
        assertEquals(ci.state("test"), Consent.State.Unknown)

        ci.grant(Feature.Lang)
        ci.grant(Feature.Ua)
        ci.grant("test")

        assertEquals(1, ci.campaignConsentSize)
        assertEquals(2, ci.featureConsentSize)

        var consent: Consent? = ci.getFeatureConsentByIndex(0)
        assertNotNull(consent)
        assertTrue(consent!!.granted)
        assertEquals(ci.state(Feature.Lang), Consent.State.Granted)

        consent = ci.getFeatureConsentByIndex(1)
        assertNotNull(consent)
        assertTrue(consent!!.granted)
        assertEquals(ci.state(Feature.Ua), Consent.State.Granted)

        consent = ci.getFeatureConsentByIndex(2)
        assertNull(consent)

        val midnightTomorrow = CleanInsights.now()
        midnightTomorrow.set(Calendar.HOUR_OF_DAY, 0)
        midnightTomorrow.set(Calendar.MINUTE, 0)
        midnightTomorrow.set(Calendar.SECOND, 0)
        midnightTomorrow.set(Calendar.MILLISECOND, 0)
        midnightTomorrow.add(Calendar.DAY_OF_MONTH, 1)

        consent = ci.getCampaignConsentByIndex(0)
        assertNotNull(consent)
        assertTrue(consent!!.start == midnightTomorrow || consent.start.after(midnightTomorrow))
        assertTrue(consent.granted)

        consent = ci.getCampaignConsentByIndex(1)
        assertNull(consent)

        // Should only ever allowed on the start of the next measurement period,
        // which should be tomorrow, as by the tested configuration.
        assertFalse(ci.isCampaignCurrentlyGranted("test"))
        assertEquals(ci.state("test"), Consent.State.NotStarted)
    }

    @Test
    fun invalidCampaign() {
        assertNull(ci.consent("foobar"))

        assertEquals(Consent.State.Unconfigured, ci.state("foobar"))

        ci.requestConsent("foobar", this) {
            assertFalse(it)
        }

        assertNull(ci.grant("foobar"))

        assertNull(ci.deny("foobar"))

        assertEquals(0, ci.campaignConsentSize)

        assertFalse(ci.isCampaignCurrentlyGranted("foobar"))
    }

    @Test
    fun persistence() {
        ci.grant(Feature.Lang)
        ci.grant(Feature.Ua)
        ci.grant("test")

        assertTrue(ci.isCampaignCurrentlyGranted("test"))

        ci.measureVisit(listOf("foo"), "test")
        ci.measureEvent("foo", "bar", "test", "baz", 4567.0)

        ci.persist()

        val midnight = CleanInsights.now()
        midnight.set(Calendar.HOUR_OF_DAY, 0)
        midnight.set(Calendar.MINUTE, 0)
        midnight.set(Calendar.SECOND, 0)
        midnight.set(Calendar.MILLISECOND, 0)

        val midnightTomorrow = CleanInsights.now()
        midnightTomorrow.set(Calendar.HOUR_OF_DAY, 0)
        midnightTomorrow.set(Calendar.MINUTE, 0)
        midnightTomorrow.set(Calendar.SECOND, 0)
        midnightTomorrow.set(Calendar.MILLISECOND, 0)
        midnightTomorrow.add(Calendar.DAY_OF_MONTH, 1)

        // Get private store property through reflection.
        val storeField = ci::class.java.getDeclaredField("store")
        storeField.isAccessible = true
        val currentStore = storeField.get(ci) as? DefaultStore

        assertEquals(arrayListOf(Visit(listOf("foo"), "test", 1, midnight, midnightTomorrow)),
            currentStore?.visits)
        assertEquals(arrayListOf(Event("foo", "bar", "baz", 4567.0, "test", 1, midnight, midnightTomorrow)),
            currentStore?.events)
    }

    @Test
    fun persistenceStrengthenedAnonymity() {
        ci = strengthenedCi

        ci.grant(Feature.Lang)
        ci.grant(Feature.Ua)
        ci.grant("test")

        ci.measureVisit(listOf("foo"), "test")
        ci.measureEvent("foo", "bar", "test", "baz", 4567.0)

        ci.persist()

        val store = storedStore

        // Get private store property through reflection.
        var storeField = ci::class.java.getDeclaredField("store")
        storeField.isAccessible = true
        var currentStore = storeField.get(ci) as? DefaultStore

        assertEquals(store.consents, currentStore?.consents)
        assertEquals(listOf<Visit>(), store.visits) // Consent will only start tomorrow!
        assertEquals(listOf<Event>(), store.events) // Consent will only start tomorrow!

        // Re-init with faked store.
        val ci = CleanInsights(confString, fakeYesterdayConsent())

        assertTrue(ci.isCampaignCurrentlyGranted("test"))

        ci.measureVisit(listOf("foo"), "test")
        ci.measureEvent("foo", "bar", "test", "baz", 4567.0)

        ci.persist()

        val midnight = CleanInsights.now()
        midnight.set(Calendar.HOUR_OF_DAY, 0)
        midnight.set(Calendar.MINUTE, 0)
        midnight.set(Calendar.SECOND, 0)
        midnight.set(Calendar.MILLISECOND, 0)

        val midnightTomorrow = CleanInsights.now()
        midnightTomorrow.set(Calendar.HOUR_OF_DAY, 0)
        midnightTomorrow.set(Calendar.MINUTE, 0)
        midnightTomorrow.set(Calendar.SECOND, 0)
        midnightTomorrow.set(Calendar.MILLISECOND, 0)
        midnightTomorrow.add(Calendar.DAY_OF_MONTH, 1)

        // Get private store property through reflection.
        storeField = ci::class.java.getDeclaredField("store")
        storeField.isAccessible = true
        currentStore = storeField.get(ci) as? DefaultStore

        assertEquals(arrayListOf(Visit(listOf("foo"), "test", 1, midnight, midnightTomorrow)),
            currentStore?.visits)
        assertEquals(arrayListOf(Event("foo", "bar", "baz", 4567.0, "test", 1, midnight, midnightTomorrow)),
            currentStore?.events)
    }

    @Test
    fun purge() {
        val store = TestStore()

        val dayBeforeYesterday = CleanInsights.now()
        dayBeforeYesterday.add(Calendar.DAY_OF_MONTH, -2)

        store.visits.add(Visit(listOf("foo"), "x", 1, dayBeforeYesterday, dayBeforeYesterday))
        store.events.add(Event("foo", "bar", null, null, "x", 1, dayBeforeYesterday, dayBeforeYesterday))

        // Get private purge method through reflection.
        val purgeMethod = Insights.Companion::class.declaredMemberFunctions.find { it.name == "purge" }
        purgeMethod?.isAccessible = true
        purgeMethod?.call(Insights.Companion, conf, store)

        assertEquals(0, store.visits.size)
        assertEquals(0, store.events.size)

        val now = CleanInsights.now()

        store.visits.add(Visit(listOf("foo"), "x", 1, now, now))
        store.events.add(Event("foo", "bar", null, null, "x", 1, now, now))

        purgeMethod?.call(Insights.Companion, conf, store)

        assertEquals(1, store.visits.size)
        assertEquals(1, store.events.size)
    }

    @Test
    fun serializeInsights() {
        var insights = Insights(1, "foo", "bar")

        assertEquals("{\"idsite\":1,\"lang\":\"foo\",\"ua\":\"bar\"}", CleanInsights.moshi.adapter(Insights::class.java).toJson(insights))

        val store = TestStore()
        store.visits.add(Visit(listOf("foo", "bar"), "test", 1,
            calendar.fromJson("2022-01-01T00:00:00Z"), calendar.fromJson("2022-01-02T00:00:00Z")))

        store.events.add(Event("foo", "bar", "baz", 6.66, "test", 1,
            calendar.fromJson("2022-01-01T00:00:00Z"), calendar.fromJson("2022-01-02T00:00:00Z")))

        val c = Configuration(conf.server, conf.siteId, conf.campaigns, conf.timeout, conf.maxRetryDelay,
            30000, conf.persistEveryNTimes, conf.serverSideAnonymousUsage, conf.debug)

        insights = Insights(c, store)

        assertEquals("{\"idsite\":1,\"visits\":[{\"action_name\":\"foo/bar\",\"period_start\":1640995200,\"period_end\":1641081600,\"times\":1}],\"events\":[{\"category\":\"foo\",\"action\":\"bar\",\"name\":\"baz\",\"value\":6.66,\"period_start\":1640995200,\"period_end\":1641081600,\"times\":1}]}",
            CleanInsights.moshi.adapter(Insights::class.java).toJson(insights))
    }

    @Test
    fun serverTest() {
        ci = CleanInsights(conf, TestStore())

        var error: Exception? = RuntimeException("error not null!")

        ci.testServer {
            error = it
        }

        assertNull(error)
    }

    @Test
    fun invalidConfigs() {
        testConfigCheck("ftp:", -1, emptyMap(), false)

        testConfigCheck("ftp:", -1, mapOf(Pair("foobar", Campaign(CleanInsights.now(), CleanInsights.now(), 0))), false)

        testConfigCheck("ftp:", 1, mapOf(Pair("foobar", Campaign(CleanInsights.now(), CleanInsights.now(), 0))), false)

        testConfigCheck("https://example.org/", 1, mapOf(Pair("foobar", Campaign(CleanInsights.now(), CleanInsights.now(), 0))), true)
    }

    @Test
    fun defaultStoreSend() = runTest {
        DefaultStore.injectedCoroutineScope = this
        val store = DefaultStore()

        var error: Exception? = RuntimeException("error not null!")

        launch {
            store.send("", URL("http://example.org/test"), 15.0) {
                error = it
            }
        }
        advanceUntilIdle()

        assertNotNull(error)
        assertTrue(error?.message?.startsWith("HTTP Error 405: Method Not Allowed") ?: false)
    }

    @Test
    fun autoTrack() = runTest {
        DefaultStore.injectedCoroutineScope = this
        @Suppress("DEPRECATION")
        val context = RuntimeEnvironment.application.applicationContext

        val storeFile = File(context.filesDir, "cleaninsights.json")
        val storeDir = storeFile.parentFile

        assertNotNull(storeDir)

        if (storeFile.exists()) {
            assertTrue(storeFile.delete())
        }

        var store = DefaultStore(hashMapOf("storageDir" to storeDir!!))

        store.consents.campaigns["visits"] = Consent(true, end = CleanInsights.now().apply {
            add(Calendar.YEAR, 1) }
        )

        store.persist(false) {
            assertNull(it)
        }

        assertNull(CleanInsights.autoTrackCi)

        launch {
            CleanInsights.autoTrack(context, conf.server, conf.siteId)
        }
        advanceUntilIdle()

        store = readStore(storeFile)

        val first = CleanInsights.now().apply {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
        }

        val last = first.copy()
        last.add(Calendar.DAY_OF_MONTH, 1)

        assertEquals( Visit(emptyList(), "visits", 1, first, last), store.visits.firstOrNull())

        assertTrue(storeFile.delete())

        assertNotNull(CleanInsights.autoTrackCi)
    }

    private val storeFile: File
        get() = File(tempDir.root, "cleaninsights.json")

    private val storedStore: DefaultStore
        get() = readStore(storeFile)

    private val strengthenedCi: CleanInsights
        get() {
            val co = conf

            val key = co.campaigns.keys.first()

            val ca = co.campaigns.values.first()

            return CleanInsights(
                Configuration(
                co.server,
                co.siteId,
                    mapOf(Pair(key, Campaign(
                        ca.start,
                        ca.end,
                        ca.aggregationPeriodLength,
                        ca.numberOfPeriods,
                        ca.onlyRecordOnce,
                        ca.eventAggregationRule,
                        strengthenAnonymity = true))),
                co.timeout,
                co.maxRetryDelay,
                co.maxAgeOfOldData,
                co.persistEveryNTimes,
                co.serverSideAnonymousUsage,
                co.debug),
                tempDir.root
            )
        }

    private fun readStore(file: File): DefaultStore {
        val json = file.readText()

        val store = CleanInsights.moshi.adapter(DefaultStore::class.java).fromJson(json)

        assertNotNull(store)

        return store!!
    }

    private fun fakeYesterdayConsent(): DefaultStore {
        val store = storedStore

        val yesterday = CleanInsights.now()
        yesterday.add(Calendar.DAY_OF_MONTH, -1)

        val end = CleanInsights.now()
        end.add(Calendar.DAY_OF_MONTH, 3)

        store.consents.campaigns["test"] = Consent(granted = true, yesterday, end)

        return store
    }

    private fun testConfigCheck(server: String, siteId: Int, campaigns: Map<String, Campaign>, isGood: Boolean) {
        val conf = Configuration(URL(server), siteId, campaigns)

        var count = 0

        val result = conf.check {
            count++
        }

        if (isGood) {
            assertEquals(0, count)
            assertTrue(result)
        }
        else {
            assertEquals(1, count)
            assertFalse(result)
        }
    }

    private class TestStore: Store() {

        override fun load(args: Map<String, Any>): Store? {
            return null
        }

        override fun persist(async: Boolean, done: (e: Exception?) -> Unit) {
            return
        }

        override fun send(data: String, server: URL, timeout: Double, done: (e: Exception?) -> Unit) {
            done(IOException("HTTP Error 400: "))
        }
    }

    override fun show(campaignId: String, campaign: Campaign, complete: ConsentRequestUiComplete) {
        // We should not end up here.
        assertTrue(false)
    }

    override fun show(feature: Feature, complete: ConsentRequestUiComplete) {
        // We should not end up here.
        assertTrue(false)
    }
}