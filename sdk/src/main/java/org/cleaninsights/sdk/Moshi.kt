/**
 * Moshi.kt
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart.
 * Copyright © 2020 Guardian Project. All rights reserved.
 */
package org.cleaninsights.sdk

import android.os.Build
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

internal class UrlAdapter {

    @ToJson
    fun toJson(url: URL): String {
        return url.toString()
    }

    @FromJson
    fun fromJson(json: String): URL {
        return URL(json)
    }
}

internal class CalendarAdapter {

    private val formatPatterns = listOf(
        // Proper ISO 8601 timezone as wanted by specification. Only available from API 24.
        "yyyy-MM-dd'T'HH:mm:ssXXX",

        // Fallback for older Android versions in case UTC timezone is just given as 'Z' letter.
        "yyyy-MM-dd'T'HH:mm:ss'Z'",

        // Further fallback for RFC 822 timezones (which doesn't have a colon separator).
        // Below we try to change the format from ISO 8601 to RFC 822 to get it parsed anyway.
        // Ridiculously enough, there seems to be now way to parse ISO 8601 timestamps
        // correctly before Android 24 without the help of now outdated libraries. D'oh!
        "yyyy-MM-dd'T'HH:mm:ssZZZZZ")

    private fun getFormatter(pattern: String): SimpleDateFormat {
        return SimpleDateFormat(pattern, Locale.US).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }
    }

    @ToJson
    fun toJson(calendar: Calendar): String {
        return getFormatter(formatPatterns[1]).format(calendar.time)
    }

    @FromJson
    fun fromJson(json: String): Calendar {
        var date: Date? = null
        val patterns = formatPatterns.toMutableList()
        var input = json

        // ISO 8601 time zone recognition is only available in API 24 and newer. Argh.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            patterns.removeAt(0)

            // Try to change ISO 8601 time zone to a RFC 822.
            input = Regex("([+-]\\d{2}):(\\d{2})$").replace(json, "$1$2")
        }

        for (pattern in patterns) {
            try {
                date = getFormatter(pattern).parse(input)
                break
            }
            catch (e: ParseException) {
                // Ignore and try the next one.
            }
        }

        val calendar = CleanInsights.now()

        // Yes, we crash here on purpose to show users, that they messed up their config files.
        calendar.timeInMillis = date!!.time

        return calendar
    }
}

internal class FeatureConsentsJsonAdapter {

    @ToJson
    fun toJson(consents: LinkedHashMap<Feature, Consent>): Map<Feature, Consent> {
        return consents
    }

    @FromJson
    fun fromJson(consents: Map<Feature, Consent>): LinkedHashMap<Feature, Consent> {
        return LinkedHashMap(consents)
    }
}

internal class CampaignConsentsJsonAdapter {

    @ToJson
    fun toJson(consents: LinkedHashMap<String, Consent>): Map<String, Consent> {
        return consents
    }

    @FromJson
    fun fromJson(consents: Map<String, Consent>): LinkedHashMap<String, Consent> {
        return LinkedHashMap(consents)
    }
}

internal class VisitListJsonAdapter {

    @ToJson
    fun toJson(list: ArrayList<Visit>): List<Visit> {
        return list
    }

    @FromJson
    fun fromJson(list: List<Visit>): ArrayList<Visit> {
        return ArrayList(list)
    }
}

internal class EventListJsonAdapter {

    @ToJson
    fun toJson(list: ArrayList<Event>): List<Event> {
        return list
    }

    @FromJson
    fun fromJson(list: List<Event>): ArrayList<Event> {
        return ArrayList(list)
    }
}

@Suppress("unused")
internal open class DataPointData(
    val period_start: Long,
    val period_end: Long,
    val times: Int)

@Suppress("unused")
internal class VisitData(
    val action_name: String,
    period_start: Long, period_end: Long, times: Int
): DataPointData(period_start, period_end, times)

@Suppress("unused")
internal class EventData(
    val category: String,
    val action: String,
    val name: String?,
    val value: Double?,
    period_start: Long, period_end: Long, times: Int
): DataPointData(period_start, period_end, times)

@Suppress("unused", "SpellCheckingInspection")
internal class InsightsData(
    val idsite: Int,
    val lang: String?,
    val ua: String?,
    val visits: List<VisitData>?,
    val events: List<EventData>?)

internal class InsightsJsonAdapter {

    @ToJson
    fun toJson(i: Insights): InsightsData {
        return InsightsData(i.idsite, i.lang, i.ua,
            i.visits.map { VisitData(it.scenePath.joinToString("/"), it.first.timeInMillis / 1000, it.last.timeInMillis / 1000, it.times) }.ifEmpty { null },
            i.events.map { EventData(it.category, it.action, it.name, it.value, it.first.timeInMillis / 1000, it.last.timeInMillis / 1000, it.times) }.ifEmpty { null })
    }
}
