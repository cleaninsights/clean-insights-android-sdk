/**
 * AutoTracker.kt
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart.
 * Copyright © 2023 Guardian Project. All rights reserved.
 */

package org.cleaninsights.sdk

import android.content.Context
import java.net.URL
import java.util.*

private var mAutoTrackCi: CleanInsights? = null

val CleanInsights.Companion.autoTrackCi: CleanInsights?
    get() = mAutoTrackCi


/**
 * Instantiates a singleton `CleanInsights` object with a default configuration
 * and a default campaign named "visits" which never expires.
 *
 * The "visits" campaign will have an `aggregationPeriodLength` of 1 day.
 * That means, starts will be collected during 24 hours and then sent to the server,
 * evenly distributed over that day, in order to keep user privacy.
 *
 * The stored data will be held in `context.filesDir`.
 *
 * After instantiation, it will automatically track a start of the app, but ONLY, if consent is given.
 *
 * You will need to show some consent form to the user, so the user
 * is able to agree to this tracking before setting consent.
 *
 * Refer to the example code to see how consent is handled.
 *
 * @param context: Any context object. Will not be retained.
 * @param server: The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.
 * @param siteId: The Matomo site ID to record page visits for.
 * @param campaigns: (Additional) campaign configurations. OPTIONAL
 * @return The instantiated `CleanInsights` object.
 */
fun CleanInsights.Companion.autoTrack(context: Context, server: URL, siteId: Int, campaigns: Map<String, Campaign>? = null): CleanInsights {
    @Suppress("NAME_SHADOWING")
    val campaigns = (campaigns ?: emptyMap()).toMutableMap()

    if (!campaigns.contains("visits")) {
        val start = now().apply { timeInMillis = 0 }

        val end = now().apply {
            set(2099, 12, 31, 23, 59, 59)
        }

        campaigns["visits"] = Campaign(start, end, aggregationPeriodLength = 1, numberOfPeriods = 50000)
    }

    val conf = Configuration(server, siteId, persistEveryNTimes = 1, debug = true, campaigns = campaigns)

    val ci = CleanInsights(conf, context.filesDir)
    mAutoTrackCi = ci

    ci.measureVisit(emptyList(), "visits")

    return ci
}